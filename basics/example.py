__author__ = 'PK'

def div(a, b):
    """Ta funkcja dodaje dwie liczby:
    np. div(3,5) = 8
    """
    return a/b;


# print div(27, 3); print div(40, 3);

# print div.__doc__

someInteger = 3
someDouble = 3.14
someBoolean = True
someDictionary = {"forename": "forename1", "surname": "surname1", "age": 31}
someTuple = ("forename2", "surname2", 32)
someList = ["forename3", "surname3", 33]

# del someTuple[0]  # tuple is not modifiable

print someList[1:3]

someList.append(34)
someList.insert(1, "someValue1")
print someList

print someList.pop()
print someList

someList.remove("forename3")
print someList

someList = ["ab", 3]*4
print someList

val = 0
val+=1
print val


# print str(someInteger) + "\n" +\
#       str(someDouble) + "\n" +\
#       str(someBoolean) + "\n" +\
#       str(someDictionary) + "\n" +\
#       str(someTuple) + "\n" +\
#       str(someList);
#
# if (1!=1):
#     print "hello"

# print someDictionary["surname"]
# print someDictionary["age"]
#
# print someTuple[0]

# for attribute in someTuple:
#     print attribute
#
# for i in range(0, len(someTuple)):
#     print someTuple[i]

# for elem in someList:
#     print elem
#
# for i in range(0, len(someList)):
#     print someList[i]

# for elem in someDictionary.values():
#     print elem
#
# for elem in someDictionary.keys():
#     print elem

# print someDictionary["age"]
#
# print someDictionary.popitem()
# print someDictionary
#
# del someDictionary["surname"]
# print someDictionary

# someDictionary["age"] = 23
# someDictionary["surname"] = "surname1"
# someDictionary["car"] = "car1"
# someDictionary["someTuple"] = (None, False, "ab", 32.11, 'a')
# someDictionary["someDictionary"] = {"foo": "foo1", "bar": None, "ab":"ab1"}
# someDictionary["someList"] = ["ab", 1, None, True]
# someDictionary["someSet"] = {"cd", 2, True}
# print someDictionary
#
# del someDictionary['someTuple']
# print someDictionary
#
# someDictionary.clear()
# print someDictionary
#
# someDictionary["car"] = "car1"
# print someDictionary

 ## str, bool, int, float, NoneType,
 ## instead of char there is 1 elem long string
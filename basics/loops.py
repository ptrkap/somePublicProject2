__author__ = 'PK'

numbers = [1, 2, 3 ,5, 6]
print numbers

for number in numbers:
    print number

mixedObjects = ["ab", 3, "cd", 4]
print mixedObjects

if 1 == 1:
    mixedObjects.append(True)
    mixedObjects.append(5)
    mixedObjects.append("ef")
print mixedObjects

if True:
    mixedObjects.remove("cd")
    mixedObjects.insert(0, "beginning")
print mixedObjects

someTuple = ("x", 11, "y", 12)
print someTuple

print someTuple.index("y", 0, 3)

for i in range(0, len(someTuple)):
    print someTuple[i]








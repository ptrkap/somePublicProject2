__author__ = 'PK'

foo = "foo1"
bar = "bar1"
someInt = 34
someFloat = 34.4534
str = "prefix_%s-%s-%d-%+.3f_postfix" % (foo, bar, someInt, someFloat)
# print str


someDictionary = {"name":"name1", "age": 30, "interests": "cars, football"}

def displayStuff(someDictionary):
    for k, v in someDictionary.items():
        print "%s-%s" % (k,v)

displayStuff(someDictionary)

listOfStrings = ["%s-%s" % (k,v) for k, v in someDictionary.items()]

listOfTuples = [(k, v, v, k) for k, v in someDictionary.items()]

listOfSets = [{k, v, v, "foo"} for k, v in someDictionary.items()]

joinedString = ";;;".join(["%s-%s" % (k,v) for k, v in someDictionary.items()])

splittedList = joinedString.split(";;;")
print
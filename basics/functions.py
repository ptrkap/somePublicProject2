__author__ = 'PK'

class Person:

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def __str__(self):
        return self.name + " - " + str(self.age)

person = Person("name1", 32)

print person.name
print person.age
print person


def fcn1(boolean):
    return boolean

def fcn2(boolean):
    return boolean


print fcn1(True)==fcn2(True)
print fcn1(True)==fcn2(False)
print fcn1(False)==fcn2(True)
print fcn1(False)==fcn2(False)


def add(a, b):
    return a+b

print add(3, 5)

if (True):
    print "This is true"

if (False):
    print "This is false"

age1 = 18

def canDriveCar(age):
    threshold = 18
    if (age > threshold):
        print "Driving a car allowed"

canDriveCar(age1)

age1+=1

canDriveCar(age1)

someStr = "some string 1"
someInt = 2
someFloat = 3.232
someArray = [1, 3, 5]
someDictionary = {"key1":"value1", "key2":"value2"}
print "someStr: %s, someInt: %s, someFloat: %s, someArray: %s, someDictionary: %s" %\
      (type(someStr), type(someInt), type(someFloat), type(someArray), type(someDictionary))




print str(someStr)
print str(someInt)
print str(someFloat)
print str(someArray)
print str(someDictionary)

print dir(someArray)


def add(a, b):
    return a + b

class Person:
    def __init__(self, name):
        self.name = name

    def introduce(self):
        print "hello, I am " + self.name


person = Person("name1")
person.introduce()


print callable(person)
print callable(person.introduce)
print callable(add)
print callable(someStr)
print callable(someInt)
print callable(someFloat)
print callable(someArray)
print callable(someDictionary)
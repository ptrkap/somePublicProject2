__author__ = 'PK'

man1 = ("manName1", 32)
# man1 = ("manName1", 32, [3, 2])
# man1 = ("manName1", 32, {2,3})
# man1 = ("manName1", {"foo": "bar"}, 32)
man2 = ("manName2", 28)

woman1 = ("womanName1", 22)
woman2 = ("womanName2", 25)

womanByMan = {man1: woman1, man2: woman2}

print womanByMan[("manName1", 32)]


tuple1 = ("ab", 1)
# tuple1 = ("ab333", 1333)
list1 = list(tuple1)
list2 = ["cd", 2]
tuple2 = tuple(list2)

print tuple1
print list1
print list2
print tuple2

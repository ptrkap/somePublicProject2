__author__ = 'PK'

tuples1 = [("ab1", 11), ("cd1", 12), ("ef1", 13), ("gh1", 14)]
tuples2 = [("ab2", 6), ("cd2", 15), ("ef2", 13), ("gh2", 17)]

print tuples1
print tuples2

def mergeByBiggestValues(tuples1, tuples2):
    output = []
    for i in range(0, len(tuples1)):
        if tuples1[i][1] > tuples2[i][1]:
            output.append(tuples1[i])
        else:
            output.append(tuples2[i])
    return output

print mergeByBiggestValues(tuples1, tuples2)
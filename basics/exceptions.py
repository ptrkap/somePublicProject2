__author__ = 'PK'

def exceptionExample1():
    try:
        raise Exception("This is some exception")
    except Exception as e:
        print "This is exception handling: " + e.message

def exceptionExample2():
    def fcn1():
        raise Exception("This exception raised in fcn1")

    def fcn2():
        fcn1()

    try:
        fcn2()
    except Exception as e:
        print "Handling: " + e.message


# exceptionExample1()
exceptionExample2()
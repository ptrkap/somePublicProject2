#-*- coding: utf-8 -*-
__author__ = 'PK'

forename = "Piotr"
surname = "Kapcia"
age = 28

message = "Hello, my name is %s %s, age %d" % (forename, surname, age)
print message


def someFcn(someParam1 = 8, someParam2 = "ab"):
    u"""
    Abc \n
    def \n
    ghi
    """
    return "%s - %s" % (someParam1, someParam2)

print someFcn()

print someFcn(3, 4)

print someFcn(6)



